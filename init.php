<?php $configuration = [
    "package" => [
        "name" => "Package Sandbox",
        "description" => "A starting point for composer package with phpunit and phpstan set.",
        "vendor" => "zrnik",
        "package" => "package-sandbox",
        "authors" => [
            [
                "name" => "Štěpán Zrník",
                "email" => "stepan@zrnik.eu",
                "homepage" => "https://stepan.zrnik.eu/"
            ]
        ]
    ],
];

// Key
$package_key = $configuration["package"]["vendor"] . "/" . $configuration["package"]["package"];

// Update JSON
$composer_json = json_decode(file_get_contents("composer.json"), true);
$composer_json["name"] = $configuration["package"]["vendor"] . "/" . $configuration["package"]["package"];
$composer_json["description"] = $configuration["package"]["description"];
$composer_json["authors"] = $configuration["package"]["authors"];

// Save JSON
$flags = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
file_put_contents("composer.json", json_encode($composer_json, $flags));

// Make Directories
@mkdir("src", 0777, true);
@mkdir("tests", 0777, true);
@mkdir("temp", 0777, true);

// Prepare Readme
$readme = '# ' . $configuration["package"]["name"] . PHP_EOL .
    '![GitHub](https://img.shields.io/github/license/' . $package_key . ')' . PHP_EOL .
    '![Packagist Downloads](https://img.shields.io/packagist/dm/' . $package_key . ')' . PHP_EOL .
    '![Travis (.com)](https://travis-ci.com/' . $package_key . '.svg?branch=master)' . PHP_EOL .
    '![Packagist Version](https://img.shields.io/packagist/v/' . $package_key . ')' . PHP_EOL.PHP_EOL.
    $configuration["package"]["description"].PHP_EOL.PHP_EOL;

//Save Readme
file_put_contents("readme.md", $readme);
