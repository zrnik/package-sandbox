# Package Sandbox
![GitHub](https://img.shields.io/github/license/zrnik/package-sandbox)
![Packagist Downloads](https://img.shields.io/packagist/dm/zrnik/package-sandbox)
![Travis (.com)](https://travis-ci.com/zrnik/package-sandbox.svg?branch=master)
![Packagist Version](https://img.shields.io/packagist/v/zrnik/package-sandbox)

A starting point for composer package with phpunit and phpstan set.

